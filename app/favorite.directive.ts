/**
 * Custom Attribute Directive
 * This will handle a setting class on the media item element
 * if it's been set to a favorite or not
 */

import { Directive, HostBinding, HostListener, Input } from '@angular/core';

@Directive({
    // square brackets because we want the selector to find a match on an element attribute
  selector: '[mwFavorite]'
})
export class FavoriteDirective {

    /**
     * To get a class applied to the host element the directive is on, you can use another angular decorator
     * (in this case host binding)
     */
        //set a css proterty (class) called .is-favorite
  @HostBinding('class.is-favorite') isFavorite = true;

  @HostBinding('class.is-favorite-hovering') hovering = false;

  @HostListener('mouseenter') onMouseEnter() {
    this.hovering = true;
  }

  @HostListener('mouseleave') onMouseLeave() {
    this.hovering = false;
  }
  
  @Input() set mwFavorite(value) {
    this.isFavorite = value;
  }
}
