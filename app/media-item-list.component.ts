import { Component } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
// used for constructor injection (of our service)
import { MediaItemService } from './media-item.service';

@Component({
  selector: 'mw-media-item-list',
  templateUrl: 'app/media-item-list.component.html',
  styleUrls: ['app/media-item-list.component.css']
})
export class MediaItemListComponent {
  medium = '';
  mediaItems = [];
  paramsSubscription;

  constructor(
    private mediaItemService: MediaItemService,
    private activatedRoute: ActivatedRoute) {}

  ngOnInit() {
    // this.mediaItems = this.mediaItemService.get
    this.paramsSubscription = this.activatedRoute.params
    // make a call to .suscribe from the mediaItemService.get method
    // suscribe takes in a function that it will call with up to 3
    // arguments (next, error, and completion): next (seen below: will be a call of media items)
      .subscribe(params => {
        let medium = params['medium'];
        if(medium.toLowerCase() === 'all') {
          medium = '';
        }
        this.getMediaItems(medium);
      });
  }

  /*

    onMediaItemDelete(mediaItem) {
      this.mediaItemService.delete(mediaItem)
    }

  */

  ngOnDestroy() {
    this.paramsSubscription.unsubscribe();
  }



  onMediaItemDelete(mediaItem) {
    this.mediaItemService.delete(mediaItem)
    // trigger a reload of the list on delete
      .subscribe(() => {
        this.getMediaItems(this.medium);
      });
  }

  getMediaItems(medium) {
    this.medium = medium;
    this.mediaItemService.get(medium)
      .subscribe(mediaItems => {
        this.mediaItems = mediaItems;
      });
  }
}
