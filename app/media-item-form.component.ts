// importing in inject decorator because we want to 
// import a datatype
// Inject decorators can be used for constructor properties
import { Component, Inject } from '@angular/core';
// import {FormGroup} from '@angular/forms';
// form builder has a group method on it for forming
// form gropus
import { Validators, FormBuilder } from '@angular/forms';
import { Router } from '@angular/router';
// importing our service to inject
import { MediaItemService } from './media-item.service';
import { lookupListToken } from './providers';

@Component({
  selector: 'mw-media-item-form',
  templateUrl: 'app/media-item-form.component.html',
  styleUrls: ['app/media-item-form.component.css']
})
export class MediaItemFormComponent {
  form; // class property

  /*
    constructor allows for constructor injection (notice we are injecting form builder into
    the onInit function)
    Now we don't need the form group and form control modules
  */

  constructor(
    private formBuilder: FormBuilder,
    private mediaItemService: MediaItemService,
    // injecting our value types with inject decorator

    /* inject decorator takes in string literal to represent object type
    and tells angular to pass lookup list value into constructor during
    constructor injection vvvvvv
    @Inject('lookupListToken') lookupList {}
    */
    @Inject(lookupListToken) public lookupLists,
    private router: Router) {}

    /**
      initializing the form event using an angular lifecycle method
      (not in constructor because it makes this code easier to unit test)
      also called our "model"
     */
  
  ngOnInit() {
    this.form = this.formBuilder.group({
      // below: fields = form controls
      medium: this.formBuilder.control('Movies'),
      // compose method takes in array of validators
      name: this.formBuilder.control('', Validators.compose([
        //both validators must pass to allow submission
        Validators.required,  // matching field must exist for validation
        Validators.pattern('[\\w\\-\\s\\/]+') // pattern validator
      ])),
      category: this.formBuilder.control(''),
      // notice above validators have paratheses and below one does not
      // and above validators were called as functions...
      // that's because above validators actually return a validator function
      // here we are handing the form control our year validator function
      year: this.formBuilder.control('', this.yearValidator),
    });
  }

  /*

These are form *controls in a form *group

ngOnInit() {
    this.form = new FormGroup({ 
      medium: new FormControl(''),
      name: new FormControl(''),
      category: new FormControl('', Validators.pattern('[\\w\\-\\s\\/]')), //second param on form control is validator
      year: new FormControl(''),
    });
  }

  */

  // custom validator (remember you're passing *controls from above)

  yearValidator(control) {
    // to demonstrate year is optional
    if (control.value.trim().length === 0) {
      return null;
    }
    // convert value into a number
    let year = parseInt(control.value);
    let minYear = 1800;
    let maxYear = 2500;
    if (year >= minYear && year <= maxYear) {
      return null;
    } else {
      return {
        'year': {
          min: minYear,
          max: maxYear
        }
      };
    }
  }

  // injecting our add media item service
  
  onSubmit(mediaItem) {
    this.mediaItemService.add(mediaItem)
      .subscribe(() => {
        this.router.navigate(['/', mediaItem.medium]);
      });
  }
}
