/**
 * Modules help keep code organized by blocks of functionality and features.
 * A root module acts as a starting point module for an angular application.
 */



// Inside curly braces is the type of (decorator?) you want to import

import { NgModule } from '@angular/core';   //decorator
import { BrowserModule } from '@angular/platform-browser';
import { ReactiveFormsModule } from '@angular/forms';
import { HttpModule, XHRBackend } from '@angular/http';

// App component will be our starting module (see file app.component)
// Note: you do not need to put extensions down
import { AppComponent } from './app.component';

import { MediaItemComponent } from './media-item.component';
import { MediaItemListComponent } from './media-item-list.component';
import { FavoriteDirective } from './favorite.directive';
import { CategoryListPipe } from './category-list.pipe';
import { MediaItemFormComponent } from './media-item-form.component'; // for model driven forms
import { MediaItemService } from './media-item.service';
import { lookupListToken, lookupLists } from './providers';
import { MockXHRBackend } from './mock-xhr-backend';
import { routing } from './app.routing';

// for injecting value types

/**
 * const = lookupList = {
 * mediums: ['Movies', 'Series']
 * }
 */

/**
 * NgModule will use
 */

/*
  Ng Root Module
 */

// We need a decorator to annotate this class so that angular knows it's a module
// + must be decorated with the NgModule decorator

@NgModule({

  /*
    Metadata: information about all kinds of ***decorators***
   */

  // bring in other angular modules that your module will need
  imports: [

    //contains core directives, pipes, and more for working with the DOM
    BrowserModule,
    ReactiveFormsModule,  // for model driven forms
    HttpModule,
    routing
  ],

  // makes components, directives, and pipes available to your module
  // that don't come from another module
  declarations: [
      // adding all of our components
    AppComponent,
    MediaItemComponent,
    MediaItemListComponent,
    FavoriteDirective,
    CategoryListPipe,
    MediaItemFormComponent  // not required
  ],

  /**
   * This is what we call "providing services" or in this case, "service".
   * lookupListToeken is for our medium data type injection
   */

  providers: [
    MediaItemService,
    { provide: lookupListToken, useValue: lookupLists },
    { provide: XHRBackend, useClass: MockXHRBackend }
  ],

  // Used for root module, and will let angular know which component or
  // components will be the starting point for the bootstrap process
  // aka 'The entry point for your app code'
  bootstrap: [
      // To what are we going to apply the boot strap call
      // aka our "starting component"
    AppComponent
  ]
})

/*
  Root class
 */

export class AppModule {}