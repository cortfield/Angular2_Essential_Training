/*
  store our concrete types that can be passed around
*/

import { OpaqueToken } from '@angular/core';

// variable to opaque token

export const lookupListToken = new OpaqueToken('lookupListToken');

export const lookupLists = {
  mediums: ['Movies', 'Series']
};