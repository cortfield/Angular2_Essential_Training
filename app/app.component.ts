import { Component } from '@angular/core';  // must import @Component decorator

// Component decorator takes in an object with known properties to configure a class
// you decorate as an angular component

/**
 * 1.) Components are really just a directive with a view
 * 2.) You are essentially writing instructions for custom
 *      DOM elements
 */


/**
 * Just like the Ng-Module decorator, the component decorator takes in a metadata object, with some
 * known properties to configure a class you decorate as an angular component
 */

@Component({

  /*
    Requires minimum of two metadata properties (selector and templateURL)
   */

  selector: 'mw-app', //used to locate a custom html element and apply the component to
  templateUrl: 'app/app.component.html', // url will be inserted
  styleUrls: ['app/app.component.css']
})
export class AppComponent {

  firstMediaItem = {
      id: 1,
      name: "Firebug",
      medium: "Series",
      category: "Science Fiction",
      year: 2010,
      watchedOn: 129416656584,
      isFavorite: false
  };
}


