/**
 * A component we want to use within the app component
 */

import { Component, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'mw-media-item',
  templateUrl: 'app/media-item.component.html',
  styleUrls: ['app/media-item.component.css']
})

/**
 * Input decorator used to expose component properties
 * (ie designed to be used on a class property)
 */

export class MediaItemComponent {
    // @ input allows binding on mw-media tags with property called mediaItem
  @Input() mediaItem;
  /*
    v   We're creating an event that can be suscribed to in our custom element
        It will allow us to Emit the delete event
   */
  @Output() delete = new EventEmitter();

  onDelete() {
      // Returns (in the argument) returns back what was requestsd to delete
      // Media item component is now wired up for an output for a delete event
      // using the output decorator
    this.delete.emit(this.mediaItem);
  }
}
