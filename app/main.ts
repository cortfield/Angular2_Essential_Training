/**
 * This is where we will bootstrap
 * ***and get the application up and running in the browser
 */

// Designating the browser as the platform
// Import contains a platform object that has a bootstrap module function (platformBrowserDynamic) on it
// ^function we will use to bootstrap our root module to the platform

//Note: you can import all sorts of things inside the curly braces
import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';
import { AppModule } from './app.module';

// platformBrowserDynamic returns instance of a platform object
platformBrowserDynamic().bootstrapModule(AppModule);

/**
 * root module (AppModule) has a bootstrap metadata property that contains a list
 * of components to use as a starting component
 */
