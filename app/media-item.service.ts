import { Injectable } from '@angular/core';
// search params class helps with packaging up seach query data
import { Http, URLSearchParams } from '@angular/http';
// the rxjs operator as mentioned below
import 'rxjs/add/operator/map';

/*
  Since the http service is a service we built, angular will not know to
  do constructor injection on it, we will use @Injectable decorator
*/
@Injectable()
export class MediaItemService {
  constructor(private http: Http) {}


  /*
    refactored to support search query values
    (let's see how HTTP service works with those)
  */

  get(medium) {
    let searchParams = new URLSearchParams();
    /*
    seach params has a method called append and pass it
    the name as a string of the parameter key,
    and the medium for the service get parameters
    as the value 
    */
    searchParams.append('medium', medium);
    /* 
    http get method returns an observable of htpp responses
    we need to unwap the http response objects that the 
    http get method sends back, **because we want service
    to return media item objects, not http response object
    that the component has to deal with. We fix that with 
    rxjs operator called 'map'. The map method expects a 
    function as an argument
    */

    // 'mediaitems' observable from http.get is called a 'cold observable'
    // meaning it won't execute until there is a call to .suscribe on it
    return this.http.get('mediaitems', { search: searchParams })
      .map(response => {
        // http response object has method called 'json' which will
        // unwrap the payload from the http call and return it as 
        // a javascript object
        return response.json().mediaItems;
      });
  }
  
  // first argument of post is a URL string 
  // (which we are setting to mediaitems URL thta the
  // mock back-end supports)
  // second argument is the body of the post
  add(mediaItem) {
    // http method supports passing different data types 
    // to them, and they will auto-detect the content type
    // and handling the content - type setter
    // (so we can import media item as is, and the 
    // underlying code, will detect it as an object 
    // and se the content type header to applications/json)
    return this.http.post('mediaitems', mediaItem)
      .map(response => {});
  }

  /*
  Delete method needs a a URL (and our mock-back end will support
  mediaItems/, then an ID)
  */
  
  delete(mediaItem) {
    return this.http.delete(`mediaitems/${mediaItem.id}`)
      .map(response => {});
  }
}
